<h2 class="appkit-anchor">InputNumber</h2>
Enter a number within certain range with the mouse or keyboard.

<h2 class="appkit-anchor">Table of contents</h2>

<ol class="number-list">
  <li><a href="#Features">Features</a></li>
  <li><a href="#How-to-use">How to use</a></li>
  <li><a href="#InputNumberProps">InputNumber Props</a></li>
  <li><a href="#InputNumberEvents">InputNumber Methods</a></li>
</ol>

<h2 class="appkit-anchor" data-hash="#Features">Features</h2>
<ol class="number-list">
  <li>When a numeric value needs to be provided.</li>
</ol>
<h2 class="appkit-anchor" data-hash="#How-to-use">How to use</h2>

##### JavaScript code

```js
//Default InputNumber

<InputNumber
  stepType="default"
  min={1}
  max={10}
  step={2}
/>

<InputNumber
  min={1}
  max={10}
  step={2}
  size="small"
/>

//Input number with arrows

<InputNumber
  stepType="arrows"
  min={1}
  max={10}
  step={2}
/>

<InputNumber
  stepType="arrows"
  min={1}
  max={10}
  step={2}
  size="small"
/>

//Input number with rounds

<InputNumber
  stepType="rounds"
  min={1}
  max={10}
  step={2}
/>

<InputNumber
  stepType="rounds"
  min={1}
  max={10}
  step={2}
  size="small"
/>

//Input number with squares

<InputNumber
  stepType="squares"
  min={1}
  max={10}
  step={2}
/>

<InputNumber
  stepType="squares"
  min={1}
  max={10}
  step={2}
  size="small"
/>

// Input number with pattern
<InputNumber
  stepType="squares"
  min={1}
  max={10}
  step={2}
  pattern={new RegExp('^\\\\d+$')}
/>

// Input number with readonly
<InputNumber
  stepType="squares"
  min={1}
  max={10}
  step={2}
  readonly
/>
```

<h2 class="appkit-anchor" data-hash="#InputNumberProps">InputNumber Props</h2>

| Property | Description                                                                                         | Type                                            | Default    | Required |
| -------- | --------------------------------------------------------------------------------------------------- | ----------------------------------------------- | ---------- | -------- |
| stepType | The different type of InputNumber.                                                                  | `string`(`default`,`arrows`,`rounds`,`squares`) | `default`  | `No`     |
| size     | The size of InputNumber.                                                                            | `string`(`default`,`small`)                     | `default`  | `No`     |
| value    | To set InputNumber value                                                                            | `number`                                        | `0`        | `No`     |
| min      | min value                                                                                           | `number`                                        | `0`        | `No`     |
| max      | max value                                                                                           | `number`                                        | `Infinity` | `No`     |
| step     | To set the step of InputNumber                                                                      | `number`                                        | `1`        | `No`     |
| integerOnly  | Only allow integer or not from manually typing/inputting.  If you want to enforce the InputNumber's value as integer, you could combine it with `step` and `value` props(e.g. set `integerOnly` as `true`, and set `step` and `value` as integer, such as 1, 2, 3, etc.).                                                                      | `bool`                                          | `true`      | `No`     |
| disabled | Disable status of InputNumber                                                                       | `bool`                                          | `false`    | `No`     |
| readonly | It prevents the user from changing the value of the field (the step functionality still available). | `bool`                                          | `false`    | `No`     |

<h2 class="appkit-anchor" data-hash="#InputNumberEvents">InputNumber Methods</h2>

| Method   | Description       | Function                         |
| -------- | ----------------- | -------------------------------- |
| onChange | Callback function | `(value: number/string) => void` |
