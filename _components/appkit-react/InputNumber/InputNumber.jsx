//@flow
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ClassName from 'classnames'
import type { CommonType } from 'appkit-react-utils/commonType'
import OutlineCirclePlus from 'appkit-react-icons/outline-circle-plus'
import OutlineCircleMinus from 'appkit-react-icons/outline-circle-minus'
import OutlinePlus from 'appkit-react-icons/outline-plus'
import OutlineMinus from 'appkit-react-icons/outline-minus'
import FillupChevron from 'appkit-react-icons/fill-up-chevron'
import FilldownChevron from 'appkit-react-icons/fill-down-chevron'
import { add, sub } from './util'

function isExist(e) {
  return e !== undefined && e !== null
}

function isNumber(num) {
  var number = Number(num) //Number function convert "" to zero
  return isExist(num) && num !== '' && !isNaN(number) && (!!number || number === 0)
}

type InputNumberProps = {
  stepType?: 'default' | 'arrows' | 'rounds' | 'squares',
  size?: 'default' | 'small',
  value?: number | string,
  step?: number | string,
  max?: number | string,
  min?: number | string,
  disabled?: boolean,
  integerOnly?: boolean,
  onChange: Function,
  readonly?: boolean,
}

type InputNumberState = {
  value: string, //store as string. need to convert to number when doing calculation
}

class InputNumber extends Component<InputNumberProps & CommonType, InputNumberState> {
  state: InputNumberState

  static defaultProps = {
    step: 1,
    max: Infinity,
    min: 0,
    value: 0,
    integerOnly: true,
  }

  constructor(props: Object) {
    super(props)
    const value = this.checkValueMinMax(props.value)
    this.state = {
      value: isNumber(value) ? value.toString() : this.defaultValue,
    }
  }

  checkValueMinMax(value: number): number {
    let result = value
    if (isNumber(this.props.min)) {
      result = Math.max(Number(this.props.min), result)
    }
    if (isNumber(this.props.max)) {
      result = Math.min(Number(this.props.max), result)
    }
    return result
  }

  componentWillReceiveProps(props: Object) {
    if (props.value != this.props.value && isNumber(props.value)) {
      this.setState({ value: props.value.toString() })
    }
  }

  onBlur = () => {
    let value = this.state.value
    if (this.isValid) {
      value = this.checkValueMinMax(Number(value))
    }
    this.setState({ value: value.toString() }, this.onChange)
  }

  onKeyDown = (e: SyntheticKeyboardEvent<*>) => {
    switch (e.keyCode) {
      case 38: // KeyUp
        e.preventDefault()
        this.increaseNumber()
        break
      case 40: // KeyDown
        e.preventDefault()
        this.decreaseNumber()
        break
      default:
        break
    }
  }

  get defaultValue(): string {
    return this.checkValueMinMax(0).toString()
  }

  get isValid(): boolean {
    return isNumber(this.state.value)
  }

  get minDisabled(): boolean {
    return !this.isValid || Number(this.state.value) - Number(this.props.step) < Number(this.props.min)
  }

  get maxDisabled(): boolean {
    return (
      this.state.value !== '' &&
      (!this.isValid || Number(this.state.value) + Number(this.props.step) > Number(this.props.max))
    )
  }

  handleChange = (e: SyntheticInputEvent<*>) => {
    const input = e.target.value
    const hasWord = /[^\d\-\.]/.exec(input)
    const invalid = hasWord || (this.props.integerOnly && input.toString().indexOf('.') > -1)
    if (!this.props.disabled && !invalid) {
      let value = this.defaultValue
      if (isNumber(input)) {
        value = input.toString()
      } else if (input === '' || input === '-' || (input === '.' && !this.props.integerOnly)) {
        value = input
      }

      this.setState(
        {
          value,
        },
        () => {
          clearTimeout(this.timeout)

          this.timeout = setTimeout(() => {
            this.onBlur()
          }, 750)
        }
      )
    }
  }

  onChange = () => {
    if (this.props.onChange) {
      this.props.onChange(Number(this.state.value))
    }
  }

  increaseNumber = (e: SyntheticKeyboardEvent<*>) => {
    const { step, max, disabled, min } = this.props
    let value = Number(this.state.value)
    if (!disabled) {
      if (!this.maxDisabled) {
        if (this.state.value === '') {
          value = this.props.min
        } else {
          if (value + Number(step) > Number(max)) return
          if (value + Number(step) < Number(min)) {
            value = Number(min) - Number(step)
          }
          value = add(value, Number(step))
        }
        this.setState({ value: value.toString() }, this.onChange)
      }
    }
    e && typeof e.preventDefault === 'function' && e.preventDefault()
  }

  decreaseNumber = (e: SyntheticKeyboardEvent<*>) => {
    const { step, min, disabled, max } = this.props
    if (!disabled) {
      let value = Number(this.state.value)
      if (!this.minDisabled) {
        if (value - Number(step) < Number(min)) return
        if (value - Number(step) > Number(max)) value = Number(max) + Number(step)

        value = sub(value, Number(step))
        this.setState({ value: value.toString() }, this.onChange)
      }
    }
    e && typeof e.preventDefault === 'function' && e.preventDefault()
  }

  handleBlur = (e: SyntheticKeyboardEvent<*>) => {
    this.props.handleBlur && this.props.onBlur()
  }

  render() {
    const {
      stepType = 'default',
      size = 'default',
      step,
      min,
      disabled,
      readonly,
      max,
      onChange,
      integerOnly,
      ...others
    } = this.props
    const { value } = this.state
    const InputNumberClasses = ClassName('input-number', {
      'number-block': stepType === 'default',
      'number-block-small': stepType === 'default' && size === 'small',
      [`number-input number-input-with-${stepType}`]: stepType !== 'default',
      'number-input-small': stepType === 'squares' && size === 'small',
    })
    const InputNumberSize = ClassName({
      'text-input': stepType !== 'default',
      'num-text': stepType === 'default',
      'text-input-small': size == 'small',
    })
    const upButtonClasses = ClassName('icon up', {
      disabled: this.maxDisabled || disabled,
    })
    const downButtonClasses = ClassName('icon down', {
      disabled: this.minDisabled || disabled,
    })
    //icon-fill-up-chevron  icon-outline-circle-plus icon-outline-plus
    let UpIcon = null,
      DownIcon = null
    switch (stepType) {
      case 'rounds':
        UpIcon = OutlineCirclePlus
        DownIcon = OutlineCircleMinus
        break
      case 'squares':
        UpIcon = OutlinePlus
        DownIcon = OutlineMinus
        break
      default:
        UpIcon = FillupChevron
        DownIcon = FilldownChevron
        break
    }

    return (
      <div className={InputNumberClasses} {...others}>
        {stepType === 'default' && <div className="num-text-hidden">{value}</div>}
        <input
          className={InputNumberSize}
          value={value}
          min={min}
          max={max}
          readOnly={readonly}
          onKeyDown={this.onKeyDown}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          type="text"
          disabled={disabled}
        />
        {stepType === 'squares' && (
          <div>
            <span className={upButtonClasses} onClick={this.increaseNumber} onKeyDown={() => {}}>
              <UpIcon />
            </span>
            <span className={downButtonClasses} onClick={this.decreaseNumber} onKeyDown={() => {}}>
              <DownIcon />
            </span>
          </div>
        )}
        {stepType !== 'squares' && (
          <div>
            <UpIcon className={upButtonClasses} onClick={this.increaseNumber} />
            <DownIcon className={downButtonClasses} onClick={this.decreaseNumber} />
          </div>
        )}
      </div>
    )
  }
}

InputNumber.propTypes = {
  stepType: PropTypes.string,
  size: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  step: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  max: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  min: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  integerOnly: PropTypes.bool,
  disabled: PropTypes.bool,
  readonly: PropTypes.bool,
  onChange: PropTypes.func,
}

export default InputNumber
