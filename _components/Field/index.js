import React from 'react'
import PropTypes from 'prop-types'
import { isNil, isEmpty } from 'ramda'
import styled from '@emotion/styled'
import Markdown from '../../../Markdown'
import Label from './Label'
import TooltipStyled from './TooltipStyled'
import TooltipIcon from './TooltipIcon'
import Text from '../Inputs/Text'
import Checkbox from '../Inputs/Checkbox'
import Int from '../Inputs/Int'
import Decimal from '../Inputs/Decimal'
import Dropdown from '../Inputs/Dropdown'
import Error from './Error'

const ComponentsMap = {
  TEXT: Text,
  CHECKBOX: Checkbox,
  INT: Int,
  DECIMAL: Decimal,
  DROPDOWN: Dropdown,
}

const Container = styled.div`
  margin-bottom: 5px;
`

const FieldFlex = styled.div`
  display: flex;
  justify-content: space-between;
`

const FieldInline = styled.div`
  display: inline-block;
`

const Field = props => {
  const { type, description, errors, label, name, isLabelTooltip, isLastItem, renderButtons } = props
  const FieldComponent = ComponentsMap[type]

  return (
    <Container>
      {type != 'CHECKBOX' && (
        <Label>
          <Markdown source={label} className={'service-item-label'} />

          {isLabelTooltip &&
            (!isNil(description) || !isEmpty(errors[name])) && (
              <TooltipStyled
                content={
                  <ul>
                    {!isNil(description) && <li>{description}</li>}
                    {errors && !isNil(errors[name]) && errors[name].map((item, index) => <li key={index}>*{item}</li>)}
                  </ul>
                }
                trigger="hover"
              >
                <TooltipIcon />
              </TooltipStyled>
            )}
        </Label>
      )}

      <FieldFlex>
        <FieldInline>
          <FieldComponent {...props} />
          {!isNil(errors[name]) && !isEmpty(errors[name]) && <Error message={errors[name][0]} />}
        </FieldInline>

        {isLastItem && renderButtons()}
      </FieldFlex>
    </Container>
  )
}

Field.propTypes = {
  values: PropTypes.object,
  name: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleFocus: PropTypes.func.isRequired,
  errors: PropTypes.object,
  isLastItem: PropTypes.bool,
  renderButtons: PropTypes.func,
}

export default Field
