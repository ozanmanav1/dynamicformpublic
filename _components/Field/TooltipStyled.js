import styled from '@emotion/styled'
import { Tooltip } from 'appkit-react'

const TooltipStyled = styled(Tooltip)`
  display: inline-block !important;
  .tooltip-inner {
    text-align: left !important;
  }
`
export default TooltipStyled
