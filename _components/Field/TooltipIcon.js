import styled from '@emotion/styled'
import FillInformation from 'appkit-react-icons/fill-information'

const TooltipIcon = styled(FillInformation)`
  cursor: pointer;
  margin-left: 5px;
  height: 12px;
  width: 12px;
`
export default TooltipIcon
