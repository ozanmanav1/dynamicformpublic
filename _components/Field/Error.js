import React from 'react'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'

const ErrorContainer = styled.div`
  margin-top: 5px;
  font-size: 12px;
  color: red;
`

const Error = ({ message }) => {
  return <ErrorContainer>{message}</ErrorContainer>
}

Error.propTypes = {
  message: PropTypes.string,
}

export default Error
