import styled from '@emotion/styled'

const Label = styled.div`
  font-size: 12px;
  font-weight: normal;
  color: #424242;
  letter-spacing: 1px;
  margin-bottom: 10px;
`

export default Label
