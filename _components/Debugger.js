import React from 'react'
import styled from '@emotion/styled'

const Container = styled.div`
  flex: 50%;
  padding: 25px;
  margin-top: 10px;
  background-color: #eee;
`

const Title = styled.div`
  font-weight: bold;
  margin-bottom: 10px;
`

const Pre = styled.pre`
  background: #f6f8fa,
  font-size: .65rem,
  padding: .5rem,
`

const Debugger = ({ data }) => {
  return (
    <Container>
      <Title>Form State:</Title>
      <Pre>{JSON.stringify(data, null, 2)}</Pre>
    </Container>
  )
}

export default Debugger
