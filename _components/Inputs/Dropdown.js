import React from 'react'
import { isEmpty } from 'ramda'
import { cx } from 'emotion'
import { Select } from 'appkit-react'
import PropTypes from 'prop-types'
import { redBorder } from './styles'

const Dropdown = ({ name, handleChange, handleBlur, errors, options, defaultValue }) => (
  <React.Fragment>
    <Select
      defaultValue={defaultValue}
      onSelect={selected => {
        handleChange(name, selected)
        handleBlur(name)
      }}
      placeholder={'Select'}
      className={cx(redBorder(!isEmpty(errors && errors[name])), 'dropdown-list-highlight')}
    >
      {options.map((entity, index) => {
        return (
          <Select.Option key={index} value={entity.value}>
            {entity.value}
          </Select.Option>
        )
      })}
    </Select>
  </React.Fragment>
)

Dropdown.propTypes = {
  placeholder: PropTypes.string.isRequired,
  defaultValue: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleFocus: PropTypes.func.isRequired,
  values: PropTypes.object,
  options: PropTypes.array,
}

export default Dropdown
