import React from 'react'
import { isEmpty } from 'ramda'
import { cx } from 'emotion'
import PropTypes from 'prop-types'
import { InputNumber } from '../appkit-react/InputNumber/index'
import { redBorder } from './styles'

const Int = ({ values, name, handleChange, handleBlur, handleFocus, errors }) => (
  <React.Fragment>
    <InputNumber
      value={values[name]}
      stepType="arrows"
      className={cx(redBorder(!isEmpty(errors && errors[name])), 'input-number number-input number-input-with-arrows')}
      step={1}
      integerOnly={true}
      onChange={value => {
        handleChange(name, value)
      }}
      onBlur={() => {
        handleBlur(name)
      }}
      onFocus={e => {
        handleFocus(name)
      }}
    />
  </React.Fragment>
)

Int.propTypes = {
  name: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleFocus: PropTypes.func.isRequired,
  values: PropTypes.object,
  errors: PropTypes.object,
}

export default Int
