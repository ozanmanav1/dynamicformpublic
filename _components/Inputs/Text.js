import React from 'react'
import { Input } from 'appkit-react'
import { isEmpty } from 'ramda'
import { cx } from 'emotion'
import PropTypes from 'prop-types'
import { redBorder } from './styles'

const Text = ({ placeholder, name, handleChange, handleBlur, handleFocus, values, errors }) => {
  return (
    <React.Fragment>
      <Input
        type="text"
        value={values[name]}
        onChange={value => {
          handleChange(name, value)
        }}
        onBlur={e => {
          handleBlur(name)
        }}
        onFocus={e => {
          handleFocus(name)
        }}
        placeholder={placeholder || ''}
        className={cx(redBorder(!isEmpty(errors && errors[name])), 'text-input')}
      />
    </React.Fragment>
  )
}

Text.propTypes = {
  placeholder: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleFocus: PropTypes.func.isRequired,

  values: PropTypes.object,
  errors: PropTypes.object,
}

export default Text
