import React from 'react'
import { Checkbox } from 'appkit-react'

import PropTypes from 'prop-types'

const CheckboxWrapped = ({ values, name, isSelected, handleChange, handleBlur, label }) => (
  <React.Fragment>
    <style>
      {`
       span.checkbox-text {
          font-size: 0.9rem !important;
        }
      `}
      >
    </style>
    <Checkbox
      value={values[name]}
      defaultChecked={isSelected}
      onChange={(event, value) => {
        handleChange(name, !(value === undefined ? isSelected : value))
        handleBlur(name)
      }}
    >
      {label}
    </Checkbox>
  </React.Fragment>
)

CheckboxWrapped.propTypes = {
  values: PropTypes.object,
  name: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleFocus: PropTypes.func.isRequired,
  label: PropTypes.string,
}

export default CheckboxWrapped
