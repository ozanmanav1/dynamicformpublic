import React from 'react'
import { InputNumber } from '../appkit-react/InputNumber/index'
import { isEmpty } from 'ramda'
import { cx } from 'emotion'
import PropTypes from 'prop-types'
import { redBorder } from './styles'

const Decimal = ({ values, name, handleChange, handleBlur, handleFocus, errors }) => (
  <React.Fragment>
    <InputNumber
      value={values[name]}
      stepType="arrows"
      className={cx(redBorder(!isEmpty(errors && errors[name])), 'input-number number-input number-input-with-arrows')}
      integerOnly={false}
      onChange={value => {
        handleChange(name, value)
        handleBlur(name)
      }}
      onBlur={() => {
        handleBlur(name)
      }}
      onFocus={e => {
        handleFocus(name)
      }}
    />
  </React.Fragment>
)

Decimal.propTypes = {
  name: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleFocus: PropTypes.func.isRequired,

  values: PropTypes.object,
}

export default Decimal
