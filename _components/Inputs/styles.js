import { css } from 'emotion'

const redBorder = isShow => css`
  border: ${isShow ? `1.5px solid red !important` : '0px'};
`

export { redBorder }
