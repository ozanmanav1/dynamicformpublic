import { keys, compose, map, fromPairs, filter, isNil, isEmpty } from 'ramda'
import { checkRegexPatternIsValid } from './utils'
import config from './config'

const validators = {
  isRequired: () => value => {
    return typeof value === 'undefined' ? config.validationMessages.required : undefined
  },
  minLength: minLength => value => {
    if ((!value && minLength > 0) || (value && minLength > 0 && minLength > value.length + 1)) {
      return config.validationMessages.minLength(minLength)
    }
  },
  maxLength: maxLength => value => value && maxLength < value.length && config.validationMessages.maxLength(maxLength),
  min: minValue => value => (minValue < value ? undefined : config.validationMessages.minValue(minValue)),
  max: maxValue => value => (maxValue > value ? undefined : config.validationMessages.maxValue(maxValue)),
  regex: (rule, fieldName) => value =>
    checkRegexPatternIsValid(rule)
      ? String(value).match(rule)
        ? undefined
        : config.validationMessages.regexTestInvalid
      : console.error(config.validationMessages.regexPatternInvalid(fieldName, rule)),
}

const getValidatorInArray = function(name, validation) {
  const arr = []
  keys(validation).forEach(function(key) {
    if (validators[key]) {
      const item = validators[key](validation[key], name)
      arr.push(item)
    }
  })
  return { fieldName: name, validations: arr }
}

const formFieldsToSchema = map(formField => getValidatorInArray(formField.name, formField.validation))

const handleErrors = schema => inputs =>
  compose(
    fromPairs,
    filter(Boolean),
    map(schemaField => {
      const { fieldName, validations } = schemaField
      return [
        fieldName,
        compose(
          filter(Boolean),
          map(validation => validation(inputs[fieldName]))
        )(validations),
      ]
    })
  )(schema)

const getErrors = (formFields, inputs) => {
  const schema = formFieldsToSchema(formFields)
  return handleErrors(schema)(inputs)
}

export { getErrors }
