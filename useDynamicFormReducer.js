import { setNestedObjectValues } from './utils'

export default function reducer(state, action) {
  switch (action.type) {
    case 'SET_VALIDATE':
      return {
        ...state,
        isValid: action.payload,
      }
    case 'FIRST_LOAD':
      return {
        ...state,
        firstLoad: false,
      }
    case 'SET_VALIDATION_ERRORS':
      return {
        ...state,
        errors: action.payload,
      }
    case 'SET_FIELD_VALUE':
      return {
        ...state,
        values: {
          ...state.values,
          ...action.payload,
        },
      }
    case 'SET_FIELD_TOUCHED':
      return {
        ...state,
        touched: {
          ...state.touched,
          ...action.payload,
        },
      }
    case 'SUBMIT_START':
      return {
        ...state,
        isSubmitting: true,
        touched: setNestedObjectValues(state.values, true),
      }
    case 'SUBMIT_SUCCESS':
      return {
        ...state,
        isSubmitting: false,
      }
    case 'SUBMIT_FAILURE':
      return {
        ...state,
        isSubmitting: false,
        submitError: action.payload,
      }
    default:
      return state
  }
}
