import React from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled'
import { useDynamicForm } from './useDynamicForm'
import Debugger from './_components/Debugger'
import Field from './_components/Field/index'

const Form = styled.form`
  flex: 50%;
  padding: 25px;
`

function DynamicForm({
  formFields,
  debug,
  formName,
  onSubmit,
  onError,
  validateOnLoad,
  onValidateChange,
  isLabelTooltip,
  renderButtons,
}) {
  const dynamicForm = useDynamicForm({
    formFields,
    validateOnLoad,
    onValidateChange: isValid => {
      if (onValidateChange) {
        onValidateChange(isValid)
      }
    },
    onError: errors => {
      if (onError) {
        onError(errors)
      }
    },
    onSubmit: async values => {
      onSubmit(values)
    },
  })

  const { handleSubmit, handleBlur, handleChange, handleFocus, errors, values } = dynamicForm

  return (
    <Form onSubmit={handleSubmit} id={formName}>
      {formFields.map((field, index) => {
        const isLastItem = formFields.length === index + 1

        return (
          <Field
            key={index}
            values={values}
            handleChange={handleChange}
            handleBlur={handleBlur}
            handleFocus={handleFocus}
            errors={errors}
            isLabelTooltip={isLabelTooltip}
            isLastItem={isLastItem}
            renderButtons={renderButtons}
            {...field}
          />
        )
      })}
      {debug && <Debugger data={dynamicForm} />}
    </Form>
  )
}

DynamicForm.propTypes = {
  config: PropTypes.object,
  formName: PropTypes.number.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onError: PropTypes.func,
  onValidateChange: PropTypes.func,
  validateOnLoad: PropTypes.bool,
  formFields: PropTypes.array.isRequired,
  debugger: PropTypes.bool,
  isLabelTooltip: PropTypes.bool,
}

DynamicForm.defaultProps = {
  isLabelTooltip: false,
  validateOnLoad: true,
  debugger: false,
}

export default DynamicForm
