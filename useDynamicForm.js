import React from 'react'
import reducer from './useDynamicFormReducer'
import { getErrors } from './validators'
import { checkAllEmpty, formatInitialValues } from './utils'

export function useDynamicForm({ formFields, validateOnLoad, onSubmit, onError, onValidateChange }) {
  const [state, dispatch] = React.useReducer(reducer, {
    values: formatInitialValues(formFields),
    validateOnLoad,
    errors: {},
    touched: {},
    isSubmitting: false,
    firstLoad: true,
  })

  React.useEffect(() => {
    if (state.validateOnLoad) {
      const errors = validate(state)
      dispatch({ type: 'SET_VALIDATION_ERRORS', payload: errors })
    }
  }, [])

  React.useEffect(
    () => {
      if (state.firstLoad) {
        dispatch({ type: 'FIRST_LOAD' })
      } else {
        const errors = validate(state)

        dispatch({ type: 'SET_VALIDATION_ERRORS', payload: errors })

        if (checkAllEmpty(errors)) {
          onValidateChange(true)

          if (state.isSubmitting) {
            try {
              onSubmit(state.values)
              dispatch({ type: 'SUBMIT_SUCCESS' })
            } catch (submitError) {
              onError(submitError)
              onValidateChange(false)
              dispatch({ type: 'SUBMIT_FAILURE', payload: submitError })
            }
          }
        } else {
          onError(errors)
          onValidateChange(false)
          dispatch({ type: 'SUBMIT_FAILURE' })
        }
      }
    },
    [state.values, state.touched, state.isSubmitting]
  )

  const validate = state => {
    console.log('Form state @@@ ->>>', state)
    const { values = {}, touched = {} } = state
    const errors = {}

    const validationErrors = getErrors(formFields, values)

    formFields.forEach(field => {
      if (state.isSubmitting || touched[field.name] || state.validateOnLoad) {
        errors[field.name] = validationErrors[field.name]
      }
    })

    return errors
  }

  const handleChange = (fieldName, value) => {
    dispatch({
      type: 'SET_FIELD_VALUE',
      payload: { [fieldName]: value },
    })
  }

  const handleFocus = (fieldName, value) => {
    // dispatch({
    //   type: 'SET_FIELD_VALUE',
    //   payload: { [fieldName]: value },
    // })
    dispatch({
      type: 'SET_FIELD_TOUCHED',
      payload: { [fieldName]: true },
    })
  }

  const handleBlur = fieldName => {
    dispatch({
      type: 'SET_FIELD_TOUCHED',
      payload: { [fieldName]: true },
    })
  }

  const handleSubmit = async event => {
    event.preventDefault()
    dispatch({ type: 'SUBMIT_START' })
  }

  return { handleChange, handleBlur, handleSubmit, handleFocus, ...state }
}
