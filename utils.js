import { compose, values, all, indexBy, prop, pipe, map } from 'ramda'

export const sleep = ms => new Promise(r => setTimeout(r, ms))

export function checkAllEmpty(obj) {
  return compose(
    all(item => item.length === 0),
    values
  )(obj)
}

export function formatInitialValues(formFields) {
  const appkitValuesMap = {
    TEXT: 'value',
    DROPDOWN: 'defaultValue',
    CHECKBOX: 'selected',
    INT: 'defaultValue',
    DECIMAL: 'defaultValue',
  }

  const getValuesByType = pipe(
    indexBy(prop('name')),
    map(item => item[appkitValuesMap[item.type]])
  )

  return getValuesByType(formFields)
}

// CODE REFERENCE : https://codesandbox.io/s/jzm3jrrr03
const isObject = obj => obj !== null && typeof obj === 'object'

export function setNestedObjectValues(object, value, visited = new WeakMap(), response = {}) {
  for (const k of Object.keys(object)) {
    const val = object[k]
    if (isObject(val)) {
      if (!visited.get(val)) {
        visited.set(val, true)
        // In order to keep array values consistent for both dot path  and
        // bracket syntax, we need to check if this is an array so that
        // this will output  { friends: [true] } and not { friends: { "0": true } }
        response[k] = Array.isArray(val) ? [] : {}
        setNestedObjectValues(val, value, visited, response[k])
      }
    } else {
      response[k] = value
    }
  }

  return response
}

export function checkRegexPatternIsValid(regex) {
  try {
    new RegExp(regex)
    return true
  } catch (error) {
    return false
  }
}
