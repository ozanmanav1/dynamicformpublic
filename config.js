export default {
  validationMessages: {
    required: 'Required',
    minLength: minLength => `At least ${minLength} characters`,
    maxLength: maxLength => `At most ${maxLength} characters`,
    minValue: minValue => `Minimum value is ${minValue}`,
    maxValue: maxValue => `Maximum value is ${maxValue}`,
    regexTestInvalid: 'Invalid entry',
    regexPatternInvalid: (fieldName, rule) => `<DynamicForm> - ${fieldName} field does not have valid regex: ${rule}`,
  },
}
